# recursive-spell-checker

A recursive spell checker that checks a bunch of files for spelling errors and generates a report when finished

## Usage
Requires python 3.5+

### Installation
```
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

### Running
```
python -m rspellcheck --dir <root directory of files to spellcheck> --ext <extension of files to examine>
```
