import os
import glob
from spellchecker import SpellChecker


class RSpellChecker:
    def __init__(self):
        self.spell_checker = SpellChecker()
        self.ignore_chars = set([
            "\r", "[", "]", ".", ":", ",", "!", "(", ")", "+", "#", "@", "$", "%", "^", "&", "*", "<", ">", "?", "/", "=", "{", "}", ";", "~", '"', "`"
        ])
        self.print_separator = "*" * 80

    def get_file_list(self, root_dir, extension):
        if not root_dir.endswith(os.path.sep):
            root_dir += os.path.sep
        file_names = []
        for file_name in glob.iglob(root_dir + "**/*" + extension, recursive=True):
            file_names.append(file_name)
        return file_names

    def strip_chars(self, contents):
        for cpattern in self.ignore_chars:
            contents = contents.replace(cpattern, "")
        return contents

    def summarize(self, reports):
        words = {}
        for report in reports:
            for misspelled in report["misspellings"]:
                if misspelled not in words:
                    words[misspelled] = set()
                words[misspelled].add(report["file"])
        return words

    def generate_raw_reports(self, root_dir, extension):
        file_list = self.get_file_list(root_dir, extension)
        reports = []
        for file_name in file_list:
            with open(file_name) as f:
                contents = f.read()
            contents = self.strip_chars(contents)
            words = contents.replace("\n", " ").replace("-", " ").replace("_", " ").split(" ")
            misspellings = self.spell_checker.unknown(words)
            misspellings.discard("")
            report = {
                "file": file_name,
                "misspellings": misspellings,
            }
            if len(misspellings) > 0:
                reports.append(report)
        return reports

    def pretty_print(self, summary):
        for word, locations in summary.items():
            print(self.print_separator)
            print("Word: " + word)
            print("")
            print("Locations:")
            for location in locations:
                print(location)
        print(self.print_separator)
        print("Summary:")
        print("")
        print("Potential Misspellings: " + str(len(summary)))
    def generate_report(self, root_dir, extension):
        self.pretty_print(self.summarize(self.generate_raw_reports(root_dir, extension)))
