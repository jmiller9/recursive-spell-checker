import argparse
from rspellcheck.spellcheck import RSpellChecker

def main():
    parser = argparse.ArgumentParser(description="Run recursive spell-check on files given a root directory and an extension.")
    parser.add_argument("--dir", dest="root_dir", type=str,
                        help="root directory to run spell checker on")
    parser.add_argument("--ext", dest="extension", type=str,
                        help="extension of files to read")
    args = parser.parse_args()
    sc = RSpellChecker()
    sc.generate_report(args.root_dir, args.extension)

if __name__ == '__main__':
    main()